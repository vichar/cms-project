class AddCustomFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :username, :string
    add_column :users, :givennames, :string
    add_column :users, :familyname, :string
    add_column :users, :altemail, :string
    add_column :users, :social, :string
    add_column :users, :phone, :string
    add_column :users, :photo, :string
    add_column :users, :icon, :string
  end
end
