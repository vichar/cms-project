class AddDetailstoComments < ActiveRecord::Migration
  def change
	add_column(:comments, :user_id, :integer)
	add_column(:comments, :post_id, :integer)
	add_column(:comments, :comment, :text)
	add_column(:comments, :type, :string)
	add_column(:comments, :comment_id, :integer) 
  end
end
