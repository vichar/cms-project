class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.string :givennames
      t.string :familyname
      t.string :altemail
      t.string :social
      t.string :web
      t.string :phone
      t.string :photo
      t.string :icon

      t.timestamps
    end
  end
end
