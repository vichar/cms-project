class Admin::PostsController < Admin::BaseController
	def index 
		@posts = Post.all
	end

	def new
		@post = Post.new
	end
	
	def edit 
		@post = Post.find(params[:id]);
	end
	
	def create
	 @post = Post.new(params[:post])
	 @post.user_id = current_user.id
	 respond_to do |format|
       if @post.save
        format.html { redirect_to(admin_posts_path, :notice => 'Post was successfully created.') }
        format.xml  { render :xml => @post, :status => :created, :location => @post }
       else
        format.html { render :action => "new" }
        format.xml  { render :xml => @post.errors, :status => :unprocessable_entity }
       end
      end
	end
	def destroy 
		@post = Post.find(params[:id])
		@post.destroy
		respond_to do |format|
    		format.html { redirect_to(admin_posts_path) }
    		format.xml  { head :ok }
  		end
	end
	def update
		@post = Post.find(params[:id])
		@post.user_id = current_user.id
		respond_to do |format|
    		if @post.update_attributes(params[:post])
      			format.html { redirect_to(admin_posts_path, :notice => 'Post was successfully updated.') }
      			format.xml  { head :ok }
    		else
      			format.html { render :action => "edit" }
      			format.xml  { render :xml => @post.errors, :status => :unprocessable_entity }
    		end
   		end
  	end
  	def show
		@post = Post.find(params[:id])
	end
	def view
		@post = Post.view
	end
end