class Admin::UsersController < Admin::BaseController
	def show
		@user = User.find(params[:id])
	end
	def index 
		@users = User.all
	end
	def admins
		@users = Admin.all
	end
	def destroy 
		@user = User.find(params[:id])
		@user.destroy
		respond_to do |format|
    		format.html { redirect_to(admin_users_path) }
    		format.xml  { head :ok }
  		end
	end
	def suppress
		@user = User.find(params[:id])
		@user.type = "User"
		@user.save
		respond_to do |format|
    		format.html { redirect_to(admin_users_path) }
    		format.xml  { head :ok }
  		end
	end
	def elevate
		@user = User.find(params[:id])
		@user.type = "Admin"
		@user.save
		respond_to do |format|
    		format.html { redirect_to(admin_users_path) }
    		format.xml  { head :ok }
  		end
	end
end
