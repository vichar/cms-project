class Admin::BaseController < ApplicationController
	before_filter :ensure_admin_user!
	def ensure_admin_user!
		unless current_user and current_user.type == "Admin"
			redirect_to(new_user_session_path)
		end
	end
end