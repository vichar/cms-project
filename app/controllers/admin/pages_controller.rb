class Admin::PagesController < Admin::BaseController
	respond_to(:js, :html)
	def index
		@pages = Page.all
	end

	def new
		@page = Page.new
	end
	
	def edit 
		@page = Page.find(params[:id]);
	end
	
	def create
	 @page = Page.new(params[:page])
	 @page.user_id = current_user.id
     @page.save
     respond_with(@page)
 
	end
	def destroy 
		@page = Page.find(params[:id])
		@page.destroy
		respond_to do |format|
    		format.html { redirect_to(admin_pages_path) }
    		format.xml  { head :ok }
  		end
	end
	def update
		@page = Page.find(params[:id])
		@page.user_id = current_user.id
		respond_to do |format|
    		if @page.update_attributes(params[:page])
      			format.html { redirect_to(admin_pages_path, :notice => 'page was successfully updated.') }
      			format.xml  { head :ok }
    		else
      			format.html { render :action => "edit" }
      			format.xml  { render :xml => @page.errors, :status => :unprocessable_entity }
    		end
   		end
  	end
  	def show
		@page = Page.find(params[:id])
	end
	def view
		@page = Page.view
	end
end