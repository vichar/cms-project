class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :username, :givennames, :familyname, :altemail, :social,:phone, :photo, :icon
  # attr_accessible :title, :body
  mount_uploader(:icon, IconUserUploader)
  mount_uploader(:photo, PhotoUploader)

  class NotAllowed < StandardError
  	def message
  		"Unauthorized Access"
  	end
  end 
end
