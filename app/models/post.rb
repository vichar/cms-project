class Post < ActiveRecord::Base
  attr_accessible :content, :title, :user_id, :icon
  mount_uploader(:icon, IconUploader)
  def creator
  		author = "";
  		if(user_id)
  			creator = User.find(user_id)
  			author = creator.givennames
  		else
  			author = "Staff Writer"
  		end

  		return author
  end
  def age_create
  	currentTime = Time.now
  	return currentTime - created_at
  end
  def age_update	

  end 
end
