 module ApplicationHelper
	def render_user_profile
		if current_user
			render 'profile'	
		else
			render 'register'
		end
	end
	def render_admin_profile
		render 'admin_profile'	
	end
	def render_profile_image(imagePath)
		image_tag(imagePath,size:"200x200")
	end
	def render_profile_icon (iconPath)
		image_tag(iconPath,size:'100x100')

	end
	def activeable_link_to_li(tag_name,text,path)
		content_tag(tag_name, class:"#{'active' if request.path == path}") do 
			link_to(text,path)
		end
	end
	def render_editable(profile_id)
		if profile_id == current_user.id
			link_to("Edit", edit_user_registration_path, class:'btn btn-primary btn-large pull-right') 
		end
	end
	def post_cover(icon)
		image_tag(icon,size:"200x200")
	end
	def image_tag(source, options={})
		source = "default.jpg" if source.blank?
		super(source, options)
	end
	def renderAdminToggleButton(usr)
		if usr.id == current_user.id
			link_to('Toggle Admin',"#", class: 'btn btn-primary btn-admin disabled')
		elsif usr.type == 'Admin'
			link_to('Toggle Admin',"/admin/users/suppress/#{usr.id}", confirm: "You are about to revoke User's Admin Access click OK to confirm otherwise click Cancel", class: 'btn btn-danger btn-admin')
		else
			link_to('Toggle Admin',"/admin/users/elevate/#{usr.id}", confirm: "You are about to grant this User, Admin Access click OK to confirm otherwise click Cancel", class: 'btn btn-primary btn-admin')
		end
	end
	def renderUserIndexDeleteButton(usr)
		if current_user.id != usr.id
			link_to("Delete","/admin/users/cancel/#{usr.id}", confirm: "Are you sure?", class: 'btn btn-danger btn-admin' )
		else
			link_to("Delete", "#" ,class: 'btn btn-danger disabled')
		end 
	end
	def renderUserIndexViewButton(usr)
		link_to('View',"/admin/users/#{usr.id}", class: 'btn btn-info btn-admin')
	end

end
