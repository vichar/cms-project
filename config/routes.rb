CMS::Application.routes.draw do
  devise_for :views
  devise_for :users
  namespace :admin do 
    resources :posts
    resources :pages
    root to: "posts#index"
    match 'users/:id' => 'users#show' 
    match 'users/' => 'users#index' 
    match 'users/cancel/:id' => 'users#destroy'
    match 'users/elevate/:id' => 'users#elevate'
    match 'users/suppress/:id' => 'users#suppress'
    match 'admins/' => 'users#admins'
  end

  resources :pages, only: [:show]
  match '/posts/all', :controller => 'posts', :action => 'all'
  match '/users/:id' => 'users#show'
  resources :posts, only: [:index, :show, :all]
  root to: "posts#index"

end
